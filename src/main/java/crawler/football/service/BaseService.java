package crawler.football.service;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.phantomjs.PhantomJSDriverService;
import org.openqa.selenium.remote.BrowserType;
import org.openqa.selenium.remote.DesiredCapabilities;

import crawler.football.common.WebDriverFactory;

public class BaseService {
	public WebDriver getWebDriver(String browserType) {
		WebDriver driver = null;
		DesiredCapabilities capabilities = new DesiredCapabilities();

		if (browserType.equals(BrowserType.PHANTOMJS)) {
			capabilities.setCapability(PhantomJSDriverService.PHANTOMJS_EXECUTABLE_PATH_PROPERTY, "E:/phantomjs.exe");

			capabilities.setJavascriptEnabled(true);
			capabilities.setCapability("takesScreenshot", false);
			capabilities.setBrowserName(BrowserType.PHANTOMJS);
			capabilities.setPlatform(Platform.ANY);

			capabilities.setCapability(PhantomJSDriverService.PHANTOMJS_CLI_ARGS,
					new String[] { "--web-security=false", "--ssl-protocol=any", "--ignore-ssl-errors=true",
							"--load-images=yes", "--disk-cache=true", "--output-encoding=utf-8" });
		} else if (browserType.equals(BrowserType.CHROME)) {
			System.setProperty("webdriver.chrome.driver", "D:/chromedriver.exe");
			ChromeOptions options = new ChromeOptions();
			// options.addArguments("-incognito");
			options.addArguments("--disable-popup-blocking");

			capabilities.setJavascriptEnabled(true);
			capabilities.setCapability("takesScreenshot", false);
			capabilities.setBrowserName(BrowserType.CHROME);
			capabilities.setPlatform(Platform.ANY);
			capabilities.setCapability(ChromeOptions.CAPABILITY, options);
		}

		driver = WebDriverFactory.getDriver(capabilities);
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

		return driver;
	}
}

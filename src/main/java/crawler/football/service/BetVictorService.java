package crawler.football.service;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.openqa.selenium.By;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.phantomjs.PhantomJSDriverService;
import org.openqa.selenium.remote.BrowserType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.google.gson.Gson;

import crawler.football.common.WebDriverFactory;
import crawler.football.model.Handicap;
import crawler.football.model.Match;
import crawler.football.model.FixOdd;
import crawler.football.model.OverUnder;

/**
 * Hello world!
 *
 */
public class BetVictorService extends BaseService {
	public static String URL = "https://www.betvictor.com/en-gb/sports/todays-matches";

	public void crawl() {
		try {
			WebDriver driver = getWebDriver(BrowserType.CHROME);
			driver.get(URL);
			WebElement webE = driver.findElement(By.cssSelector("#full_width_head_content > div > div"));
			Actions action = new Actions(driver);
			action.moveToElement(webE).perform();
			driver.findElement(By.cssSelector("li.odds-selector-component__item:nth-child(3) > a:nth-child(1)"))
					.click();
			List<Match> listMatch = new ArrayList<Match>();

			List<WebElement> eTable = driver.findElements(
					By.cssSelector("#three_way_outright_coupon-markets > table:nth-child(2) > tbody > tr"));
			for (WebElement webElement : eTable) {
				try {
					Match match = new Match();
					match.setTime(webElement.findElement(By.cssSelector("td.date")).getText());
					String desc = webElement.findElement(By.cssSelector("td.event_description")).getText();
					String[] matchTeam = desc.split(" v ");
					match.setTeam1(matchTeam[0]);
					match.setTeam2(matchTeam[1]);
					match.setLeague(webElement.findElement(By.cssSelector("td.event_description > span")).getText());
					FixOdd matchFixOdd = new FixOdd();
					match.setNextURL(
							webElement.findElement(By.cssSelector("td.event_description>a")).getAttribute("href"));
					match.setMatchFixOdd(matchFixOdd);
					listMatch.add(match);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}

			for (Match match : listMatch) {
				try {
					driver.get(match.getNextURL());
					FixOdd matchFixOdd = new FixOdd();

					matchFixOdd.setWin(Double.parseDouble(driver
							.findElement(By.cssSelector(
									"#center_content > div:nth-child(4)>div>table>tbody>tr>td:nth-child(1)"))
							.getAttribute("data-sort")));

					matchFixOdd.setDraw(Double.parseDouble(driver
							.findElement(By.cssSelector(
									"#center_content > div:nth-child(4)>div>table>tbody>tr>td:nth-child(2)"))
							.getAttribute("data-sort")));

					matchFixOdd.setLost(Double.parseDouble(driver
							.findElement(By.cssSelector(
									"#center_content > div:nth-child(4)>div>table>tbody>tr>td:nth-child(3)"))
							.getAttribute("data-sort")));

					match.setMatchFixOdd(matchFixOdd);

					OverUnder overUnder = new OverUnder();

					String desc = driver
							.findElement(By.cssSelector(
									"#center_content > div:nth-child(12)>div>table>tbody>tr>td:nth-child(1)>span"))
							.getAttribute("data-outcome_description");
					overUnder.setTotalGoal(desc.replaceAll("Over", "").trim());
					overUnder.setOver(driver
							.findElement(By.cssSelector(
									"#center_content > div:nth-child(12)>div>table>tbody>tr>td:nth-child(1)"))
							.getAttribute("data-sort"));
					overUnder.setUnder(driver
							.findElement(By.cssSelector(
									"#center_content > div:nth-child(12)>div>table>tbody>tr>td:nth-child(2)"))
							.getAttribute("data-sort"));
					match.setOverUnder(overUnder);

					List<WebElement> elements = driver
							.findElements(By.cssSelector("#center_content > div.coupon_grouping_section > ul > li>a"));
					WebElement eHandicap = null;
					for (WebElement webElement2 : elements) {
						if (webElement2.getText().toUpperCase().contains("HANDICAPS")) {
							eHandicap = webElement2;
							break;
						}
					}
					if (eHandicap != null) {
						eHandicap.click();
					}

					List<WebElement> listTitle = driver
							.findElements(By.cssSelector("#center_content>div.single_markets>div"));
					List<Handicap> handicaps = new ArrayList<Handicap>();
					for (WebElement webElement : listTitle) {
						WebElement eDiv = webElement.findElement(By.cssSelector("h4"));
						if (eDiv != null) {
							String title = eDiv.getAttribute("title");
							if (title.contains("Asian Handicap") && title.contains("90 Mins")) {

								List<WebElement> eHandicaps = webElement.findElements(By.cssSelector("table>tbody>tr"));
								for (WebElement webElement2 : eHandicaps) {
									Handicap handicap = new Handicap();
									WebElement e1 = webElement2.findElement(By.cssSelector("td:nth-child(1)"));
									WebElement e2 = webElement2.findElement(By.cssSelector("td:nth-child(2)"));
									desc = e1.findElement(By.cssSelector("td:nth-child(1)>span")).getAttribute("data-outcome_description");
									desc = desc.substring(desc.indexOf("(")+1, desc.indexOf(")"));
									handicap.setGoal(desc);
									handicap.setTeam1(e1.getAttribute("data-sort"));
									handicap.setTeam2(e2.getAttribute("data-sort"));
									handicaps.add(handicap);
								}
							}
						}
					}
					match.setHandicap(handicaps);
					Gson gson = new Gson();
					System.out.println(gson.toJson(match));

				} catch (Exception e) {
					e.printStackTrace();
				}

			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}

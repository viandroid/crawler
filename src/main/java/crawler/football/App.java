package crawler.football;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.openqa.selenium.By;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.phantomjs.PhantomJSDriverService;
import org.openqa.selenium.remote.BrowserType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.google.gson.Gson;

import crawler.football.common.WebDriverFactory;
import crawler.football.model.Handicap;
import crawler.football.model.Match;
import crawler.football.model.FixOdd;
import crawler.football.model.OverUnder;
import crawler.football.service.BetVictorService;

/**
 * Hello world!
 *
 */
public class App {
	private static BetVictorService betVictorService;

	public static void main(String[] args) {
		betVictorService = new BetVictorService();
		betVictorService.crawl();
	}
}

package crawler.football.model;

public class FixOdd {
	private Double win;
	private Double draw;
	private Double lost;
	public Double getWin() {
		return win;
	}
	public void setWin(Double win) {
		this.win = win;
	}
	public Double getDraw() {
		return draw;
	}
	public void setDraw(Double draw) {
		this.draw = draw;
	}
	public Double getLost() {
		return lost;
	}
	public void setLost(Double lost) {
		this.lost = lost;
	}

}

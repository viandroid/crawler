package crawler.football.model;

import java.util.List;

public class Match {
	private String time;
	private String team1;
	private String team2;
	private String league;
	private FixOdd fixOdd;
	private OverUnder overUnder;
	private List<Handicap> handicap;
	private transient String nextURL;

	public List<Handicap> getHandicap() {
		return handicap;
	}

	public void setHandicap(List<Handicap> handicap) {
		this.handicap = handicap;
	}

	public OverUnder getOverUnder() {
		return overUnder;
	}

	public void setOverUnder(OverUnder overUnder) {
		this.overUnder = overUnder;
	}

	public String getNextURL() {
		return nextURL;
	}

	public void setNextURL(String nextURL) {
		this.nextURL = nextURL;
	}

	public String getLeague() {
		return league;
	}

	public void setLeague(String league) {
		this.league = league;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public FixOdd getMatchFixOdd() {
		return fixOdd;
	}

	public void setMatchFixOdd(FixOdd matchFixOdd) {
		this.fixOdd = matchFixOdd;
	}

	public String getTeam1() {
		return team1;
	}

	public void setTeam1(String team1) {
		this.team1 = team1;
	}

	public String getTeam2() {
		return team2;
	}

	public void setTeam2(String team2) {
		this.team2 = team2;
	}
}
